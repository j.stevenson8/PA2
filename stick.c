#include <stdio.h>
/*
John Stevenson
CSE 224
PA2
10/17/19

This program plays the stick game. 
The program accepts a command line argument for the number of sticks the user would like to play with.
If no such argument is presented, the program will prompt the user to input the number of sticks they would like to play with.
At any point in picking the size of the initial pile of sticks, if the user enters an invalid response, the program will print an error message and terminate.
If the user inputs a valid response, the program will jump to the user's turn. 
It will prompt the user to enter the number of sticks they would like to pick.
If an incorrect response is given, the program will print an error message and prompt the user to try again.
It will continue to print error messages until a correct response is given.
Once a correct response is given, the program subtracts this choice from the pile and prints out the number of remaining sticks.
Then it will check to see if the number of sticks remaining is less than or equal to zero, indicating that the game is over.
If it is, it will print a message letting the user know who won and then terminate.
If it isn't, the program will move on to the cpu turn.
The cpu will pick <sticks % 4> unless that number is zero.
In that case, it will pick one stick.
It will then check to see that there are sticks left. 
The game will loop until there are no sticks left and a winner has been declared.
This program plays the stick game. 
The program accepts a command line argument for the number of sticks the user would like to play with.
If no such argument is presented, the program will prompt the user to input the number of sticks they would like to play with.
At any point in picking the size of the initial pile of sticks, if the user enters an invalid response, the program will print an error message and terminate.
*/


int command_line_arguments(int );
int ask_for_sticks(int *);
int command_line_validity(char ** , int *);
int user_turn(); 
int print_sticks(int, int);
int is_game_over(int);
int cpu_turn(int);

int main(int argc, char **argv)
{
	int result, game_over, choice, sticks;
	
	game_over = 0;

	//Checks to see how many command line arguments are present
	result = command_line_arguments(argc);

	if (result == -1)
	{
		//If there are more than one command line arguments, notify the user and terminate the program
		printf("\n\n\nError\nInvalid number of command line arguments\nProgram terminated\n\n\n");
		return 0;
	}
	else if (result == 0)
	{
		//If no command line argument is given, call the function to ask the user how many sticks they would like to start with
		result = ask_for_sticks(&sticks);
			
		if (result == -1)
		{
			return 0;
		}
	}
	else
	{
		//If there is only one command line argument, check the argument for validity 
		result = command_line_validity(argv, &sticks);

		if (result == -1)
		{
			return 0;
		} 			
		
	}

	while (1)
	{
		sticks = print_sticks(user_turn(), sticks);
	
		if (is_game_over(sticks) == 1)
		{
			printf("\n\n\nThere are no sticks left\nYou win!\n\n\n");
	
			return 0;
		}	

		sticks = print_sticks(cpu_turn(sticks), sticks);
		
		if (is_game_over(sticks) == 1)
		{
			printf("\n\n\nThere are no sticks left\nI win!\n\n\n");
	
			return 0;
		}
	}

	return 0;
}
//checks for number of command line arguments
int command_line_arguments(int argc)
{
	if (argc == 2)
		return 1;  //If one command line argument is present, return 1
	else if (argc == 1)
		return 0;  //If no command line arguments are present, return 0
	else 
		return -1; //If more than one command line argument is present, return -1
}

//asks user for sticks and checks the validity of their response 
int ask_for_sticks(int *sticks)
{
	char input[100], extra[100];
	int result;

	printf("\n\n\nWelcome to the matchstick game!\nYou and I will take turns taking 1, 2, or 3 matchsticks from the pile.\nThe object of the game is to take the last stick\n\n\n");
	printf("Please enter the number of sticks you would like to start with: ");

	fgets(input, 99, stdin);
	
	result = sscanf(input, "%d%s", sticks, extra);

	if (result == 1)
	{
		if (*sticks >= 10)
		{
			printf("\n\nYou chose to play with %d sticks\n\n\n", *sticks);
			return 1;
		}
		else 
		{
			printf("\n\nError\nInvalid input\nMust enter a positive integer value greater than 10\n\n\n");
			return -1;
		}
	}
	else 
	{
		printf("\n\nError\nInvalid input\nMust enter a positive integer value greater than 10\n\n\n");
		return -1;
	}

}
//checks command line argument for validity
int command_line_validity(char **argv, int *sticks)
{
	int result;
	char extra[100];

	result = sscanf(argv[1], "%d%s", sticks, extra);

	if (result == 1)
	{
		if (*sticks >= 10)
		{
			printf("\n\n\nYou choose to play with %d sticks\n\n\n", *sticks);
			return 1;
		}
	}
	else 
	{
		printf("\n\n\nError\nInvalid input\nMust enter a positive integer value greater than 10\n\n\n");
		return -1; 
	}
}
//asks user for number of sticks and checks their response for validity
int user_turn()
{
	int num, result;
	char input[100], extra[100];

	printf("\n\n\nIt is your turn\nHow many sticks would you like to take? ");

	while (1)
	{
		fgets(input, 100, stdin);
		result = sscanf(input, "%d%s", &num, extra);
	
		if (result == 1)
		{
			if (num <= 3 && num >= 1)
			{
				printf("\n\n\nYou choose to take %d sticks\n", num);
				return num;
			}
			else
			{
				printf("\n\n\nError\nYou must choose 1, 2, or 3 sticks\nPlease try again\n");
			}
		}
		else 
		{
			printf("\n\n\nError\nYou must choose 1, 2, or 3 sticks\nPlease try again\n");
		}

	}
}
//prints the number of sticks remainind
int print_sticks(int choice, int sticks)
{
	int i = 0;

	sticks = sticks - choice;

	while (i < sticks)
	{
		printf("|");
		i++;
	}
	
	printf("(%d)\n\n\n", sticks);

	return sticks;
}
//checks to see if the game is over 
int is_game_over(int sticks)
{
	if (sticks <= 0)
	{
		return 1;
	}
	else 
	{
		return 0;
	}
}
//decides how manyu sticks the cpu will take
int cpu_turn(int sticks)
{
	int choice = sticks % 4; 

	if (choice == 0)
	{
		printf("I choose to take 1 stick\n");
		return 1;
	}
	else 
	{
		printf("I choose to take %d sticks\n", choice);
		return choice;
	}
} 
