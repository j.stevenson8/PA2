If the user inputs a valid response, the program will jump to the user's turn. 
It will prompt the user to enter the number of sticks they would like to pick.
If an incorrect response is given, the program will print an error message and prompt the user to try again.
It will continue to print error messages until a correct response is given.
Once a correct response is given, the program subtracts this choice from the pile and prints out the number of remaining sticks.
Then it will check to see if the number of sticks remaining is less than or equal to zero, indicating that the game is over.
If it is, it will print a message letting the user know who won and then terminate.
If it isn't, the program will move on to the cpu turn.
The cpu will pick <sticks % 4> unless that number is zero.
In that case, it will pick one stick.
It will then check to see that there are sticks left. 
The game will loop until there are no sticks left and a winner has been declared.
